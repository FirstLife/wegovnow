---
title: "Inputmap"
date: 2019-03-26T19:51:32+01:00
draft: false
---

The InputMap provides a unified system for spatial input in WeGovNow. It is meant to enhance the overall look and feel of the WeGovNow platform, providing an alternative to the different input methods used within WeGovNow components. Moreover, the InputMap, working in combination with OnToMap, has the role to consolidate the data within WeGovNow, providing a mechanism to create explicit connections between components’ data and entities in OnToMap.
The InputMap is web map based on Leaflet >= 1 and Angular >= 2 to collect enhanced spatial information as an input on a map. The map output is a triple (x, y, z) equivalent to latitude, longitude and map zoom level. Moreover, the output indicates the ID and URI of the relevant geographical entity if available.


##### Source code  repository link: 
__https://gitlab.di.unito.it/wgn/inputmap__
