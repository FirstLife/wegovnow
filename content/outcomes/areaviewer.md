---
title: "Areaviewer"
date: 2019-03-26T19:51:23+01:00
draft: false
---

The AreaViewer is a web map based application providing a view of WeGovNow aggregated data based on OnToMap. It works in combination with the InputMap component, exploiting the explicit relations between application data of WeGovNow components and OnToMap entities. The AreaViewer is a component of the LandingPage that can be included in any WeGovNow component trough iFrame (embed) and be controlled via url. The purposes of the AreaViewer are enhancing the overall look and feel of WeGovNow platform and providing a coherent visualisation of the current status of WeGovNow instances across components. It is web map based on Leaflet 1x. and Angular 2.x.

##### Source code  repository link: 
__https://gitlab.di.unito.it/wgn/areaviewer__
