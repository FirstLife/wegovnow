---
title: "Landing Page"
date: 2019-03-26T19:50:57+01:00
draft: false
---

The WGN Landing Page is a WeGovNow platform component which displays data about user activities that are collected by the other front-end applications of the platform; data is sent to the OTM LOGGER by the front-end applications themselves and it is showed on the WGN Landing Page .
The WGN Landing Page displays exclusively public, geographic data; it doesn’t verify neither the quality nor the appropriateness of the data sent to it, since this task is demanded to the other front-end applications originally collecting the data.
The WGN Landing Page doesn’t have access to any personal data belonging to the users, after the user logged in WGN Landing Page receives numeric user id (UWUM ID ) and username , data is used to display username on the navigation bar and to associate the logged events to the users generating them through their actions (e.g., the editing of a geographic piece of data).

##### Source code  repository link: 
__https://gitlab.di.unito.it/wgn/landing-page__
