---
title: "Firstlife Tile Sever"
date: 2019-03-26T19:51:46+01:00
draft: false
---

Sources of cartographic information are indexed as tiles (x, y, z) with the format of SVG, GeoJSON or image (PNG, JPEG). The source can be locally stored or remotely retrieved (handled by the local storage).

##### Source code  repository link: 
__https://gitlab.di.unito.it/wgn/tileserver__

