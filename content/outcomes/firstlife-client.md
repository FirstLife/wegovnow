---
title: "Firstlife Client"
date: 2019-03-26T19:50:26+01:00
draft: false
---

The client is a map-based web application relying on well-known technologies like LeafletJs and AngularJS. Users can interact with the map by searching initiatives in a specific area, creating entities and enriching them by adding comments, descriptions and images. As example, in the following figure is depicted a common use of FirstLIfe in which the user:

- interacts with the map in order to find the area he is interested in
- opens the wall in order to read the titles of the displayed entities
- clicks on an entity to
- opens a card
- adds a description

##### Source code  repository link: 
__https://gitlab.di.unito.it/wgn/firstlife-client__
